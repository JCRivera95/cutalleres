<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Workshop;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'workshop_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workshops() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Workshop::class);
    }

    /**
     * @param String $pass
     * @return array
     */
    public function checkPassword(String $pass) : array
    {
        try{
            $response = ['status' => Hash::check($pass,self::getAuthPassword()), 'message' => ''];
            if(!$response['status']) $response['message'] = 'Tu contraseña actual es incorrecta';
            return $response;
        }catch (\Exception $e){
            return ['status' => false, 'message' => 'Ocurrió un problema, intentalo más tarde'];
        }
    }

    public static function getWorkshop()
    {
        if(Auth::user()->type == 2){
            $workshop = Workshop::where('user_id',Auth::user()->id)->first();
            $workshop->schedule = json_decode($workshop->schedule);
            return $workshop;
        }
    }




}
