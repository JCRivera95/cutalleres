<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use Validator;


class ProfileController extends Controller
{
    private static $error = "error";
    private static $success = "success";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $workshop = User::getWorkshop();
//        dd($workshop->schedule->monday_start);
        return view('users.profile',['workshop' => $workshop]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($validator->fails()) return redirect()->route('perfil.index')->withErrors($validator)->withInput();

        $user = Auth::user();
        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();
        $request->avatar->storeAs('avatars',$avatarName);
        $user->avatar = $avatarName;

        try {
            $user->save();
            $message = "Tú foto de perfil se a actulizado correctamente";
            $type = self::$success;
        } catch (\Exception $e) {
            var_dump($e);
            return ['status' => false, 'message' => 'Ocurrió un problema, intentalo más tarde'];
        }
        return redirect()->route('perfil.index')->with($type,$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'nullable',
            'new_password' => 'required_with:password|confirmed'
        ]);

        if ($validator->fails()) return redirect()->route('perfil.index')->withErrors($validator)->withInput();

        $user = User::findOrFail($id);
        if (isset($request->password)){
            $response = $user->checkPassword($request->password);
            if(!$response['status']){
                return redirect()->route('perfil.index')->with($type = self::$error,$response['message']);
            }
            $user->password = bcrypt($request->new_password);
        }
        try{
            $user->save();
            $message = "Tú perfil se a actulizado correctamente";
            $type = self::$success;
        }catch (\Exception $e){
            $message = "Ha ocurrido un error, por favor intentelo más tarde";
            $type = self::$error;
        }
        return redirect()->route('perfil.index')->with($type,$message);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
