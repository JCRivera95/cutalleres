<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Workshop;

class WorkshopController extends Controller
{
    private static $error = "error";
    private static $success = "success";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workshops =  Workshop::all();
        return view('home',['workshops' => $workshops]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $taller = Workshop::find($id);
        $taller->schedule = json_decode($taller->schedule);
        return view('users.workshopDetail',['taller' => $taller]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //dd($request->email);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'required',
            'email' => 'nullable|email|unique:workshops,email',
            'phone' => 'nullable|numeric'
        ]);
        if ($validator->fails()) return redirect()->route('perfil.index')->withErrors($validator)->withInput();
        $schedule = [
            'monday_start' => $request->monday_schedule_start,
            'monday_end' => $request->monday_schedule_end,
            'tuesday_start' => $request->tuesday_schedule_start,
            'tuesday_end' => $request->tuesday_schedule_end,
            'wednesday_start' => $request->wednesday_schedule_start,
            'wednesday_end' => $request->wednesday_schedule_end,
            'thursday_start' => $request->wednesday_schedule_start,
            'thursday_end' => $request->thursday_schedule_end,
            'friday_start' => $request->friday_schedule_start,
            'friday_end' => $request->friday_schedule_end
        ];
        $workshop = Workshop::findOrFail($id);
        try{
            $workshop->update($request->all() + ['schedule' =>json_encode($schedule)]);
            $message = "Tú perfil se a actulizado correctamente";
            $type = self::$success;
        }catch (\Exception $e){
            $message = "Ha ocurrido un error, por favor intentelo más tarde";
            $type = self::$error;
        }
        return redirect()->route('perfil.index')->with($type,$message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
