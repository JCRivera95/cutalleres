<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Workshop;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/talleres';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'type' => ['required', 'string', 'max:255'],
            'workshop_name' => ['string', 'max:255', 'nullable'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        try{
            $user =  User::create([
                'type' => $data['type'],
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            if($data['type'] == 2) {
                Workshop::create([
                    'name' => $data['workshop_name'],
                    'user_id' => $user->id,
                    'schedule' => '{"monday_start":"S\/H","monday_end":"S\/H","tuesday_start":"S\/H","tuesday_end":"S\/H","wednesday_start":"S\/H","wednesday_end":"S\/H","thursday_start":"S\/H","thursday_end":"S\/H","friday_start":"S\/H","friday_end":"S\/H"}'
                ]);
            }
            return $user;


        }catch (\Exception $e){
            return redirect('register')->with('error','Se ha producido un error, por favor intetalo más tarde')->withInput();
        }

    }
}
