<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CuTalleres</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700,900" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>

            html, body {
                background:  url(/../img/bg-welcome.png) no-repeat center center;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                color: #FFF;
                font-family: 'Titillium Web', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow: hidden;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: flex-start;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .links {
                opacity: 0;
                text-align: center;
            }

            .title {
                margin: 25px 0;
                text-align: center;
                font-size: 4rem;
                cursor: default;
                opacity: 0;
                background-color: rgba(73, 167, 93, 0.51);
            }

            .title > p {
                font-weight: bold;
                padding: 10px;
                margin: 0;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
            
            .links .custom-btn {
                margin: 10px 20px;
                text-decoration: none;
                color: #5db480 !important;
                text-transform: uppercase;
                background: #ffffff;
                padding: 10px 20px;
                width: 150px;
                border: 2px solid #5db480 !important;
                border-radius: 5rem;
                display: inline-block;
                transition: all 0.4s ease 0s;
                font-weight: bold;
            }

            .custom-btn:hover {
                color: #ffffff !important;
                background: #5db480 ;
            }

            @keyframes slideInFromTop {
                0% {
                    transform: translateY(-1000%);
                    opacity: 0;
                }
                100% {
                    transform: translateY(0);
                    opacity: 1;
                }
            }

            .title {  
                animation: slideInFromTop 1s .5s cubic-bezier(0, .5, 0, 1) forwards;
            }

            .links {
                animation: slideInFromTop 1s .5s cubic-bezier(0, .5, 0, 1) forwards;
            }

        </style>
    </head>
    <body>
        <div class="title">
            <P>Bienvenido a CuTalleres</P>
        </div>
        @if (Route::has('login'))
            <div class="links">
                @auth
                    <a class="custom-btn" href="{{ url('/talleres') }}">Inicio</a>
                @else
                    <a class="custom-btn" href="{{ route('login') }}">Iniciar sesión</a>
                    @if (Route::has('register'))
                        <a class="custom-btn" href="{{ route('register') }}">Registro</a>
                    @endif
                @endauth
            </div>
        @endif
    </body>
</html>
