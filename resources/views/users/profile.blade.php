@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 mx-auto">
                <div class="row profile-card mt-4">
                    <div class="col-md-4">
                        <div class="profile-img text-center mb-5">
                            <img src="{{ asset('/storage/avatars/'.Auth::user()->avatar) }}" onerror="this.src='{{ asset('/img/placeholder_profile.png') }}';" alt="profile-img">
                            <form action="{{ route('perfil.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <input class="mt-3" type="file" name="avatar" id="avatarFile" aria-describedby="fileHelp" required>
                                    <small id="fileHelp" class="form-text text-muted mt-2">Por favor elija una imagen válida y no mayor a 2MB.</small>
                                </div>
                                <button type="submit" class="custom-btn btn-block">Cambiar foto de perfil</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8">

                        <h3 class="mb-4">Mis datos</h3>

                        <div class="form-label-group">
                            <input id="name" type="text" class="form-control" name="name" value="{{Auth::user()->name}}"
                                   disabled>
                            <label for="name">{{ __('Nombre') }}</label>
                        </div>

                        <div class="form-label-group">
                            <input id="email" type="email" class="form-control" name="email"
                                   value="{{Auth::user()->email}}"
                                   disabled>
                            <label for="email">{{ __('Correo eléctronico') }}</label>
                        </div>

                        <hr>
                        <h3 class="mb-4">Cambiar contraseña</h3>

                        <form action="{{ route('perfil.update', Auth::user()->id) }}" method="POST"
                              enctype="multipart/form-data">
                            <div class="form-label-group">
                                @csrf
                                @method('PUT')
                                <input id="password" type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password"
                                       autocomplete="off">
                                <label for="password">{{ __('Contraseña') }}</label>
                            </div>

                            <div class="form-label-group">
                                <input id="new-password" type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="new_password" autocomplete="off">
                                <label for="password">{{ __('Nueva contraseña') }}</label>
                            </div>

                            <div class="form-label-group">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="new_password_confirmation">
                                <label for="password-confirm">{{ __('Confirmar contraseña') }}</label>
                            </div>
                            <button class="custom-btn btn-block" type="submit">Guardar</button>

                        </form>
                    </div>
                    @if (Auth::user()->type == 2)

                    <div class="col-md-8 offset-md-4">
                            <hr>
                            <h3 class="mb-4">Datos de mi taller</h3>

                            <form action="{{ route('talleres.update',$workshop->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-label-group">
                                    <input id="workshop_name" type="text"
                                           class="form-control{{ $errors->has('workshop_name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ $workshop->name }}">
                                    <label for="workshop_name">{{ __('Nombre del taller') }}</label>
                                </div>

                                <div class="form-label-group">
                                    <textarea name="description" id="workshop_description"
                                              rows="3">{{ $workshop->description }}</textarea>
                                    <label for="">{{ __('Descipción del taller') }}</label>
                                </div>

                                <div class="form-label-group">
                                    <input id="workshop_contact_phone" type="text"
                                           class="form-control{{ $errors->has('workshop_contact_phone') ? ' is-invalid' : '' }}"
                                           name="phone" value="{{ $workshop->phone }}">
                                    <label for="workshop_contact_phone">{{ __('Número de contacto') }}</label>
                                </div>

                                <div class="form-label-group">
                                    <input id="workshop_contact_mail" type="text"
                                           class="form-control{{ $errors->has('workshop_contact_mail') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ $workshop->email }}">
                                    <label for="workshop_contact_mail">{{ __('Email de contacto') }}</label>
                                </div>
                                <h5 class="text-center mb-4">Horarios de mi taller<br>
                                    <small class="text-muted">Ingrese horarios con formato HH:MM AM/PM.</small>
                                    <br>
                                    <small class="text-muted font-weight-bold">Si su taller no tiene horario en uno de los
                                        días, 
                                        deje el valor por defecto.
                                    </small>
                                </h5>

                                <div class="row text-center">
                                    <div class="col-md-2">
                                        <h3 class="m-2">Lunes</h3>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="monday_schedule_start" type="text"
                                                   value="{{ $workshop->schedule->monday_start }}"
                                                   class="text-center form-control{{ $errors->has('monday_schedule_start') ? ' is-invalid' : '' }}"
                                                   name="monday_schedule_start">
                                            <label for="monday_schedule_start">{{ __('Inicio') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="monday_schedule_end" type="text"
                                                   value="{{ $workshop->schedule->monday_end }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('monday_schedule_end') ? ' is-invalid' : '' }}"
                                                   name="monday_schedule_end">
                                            <label for="monday_schedule_end">{{ __('Fin') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-md-2">
                                        <h3 class="m-2">Martes</h3>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="tuesday_schedule_start" type="text"
                                                   value="{{ $workshop->schedule->tuesday_start }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('tuesday_schedule_start') ? ' is-invalid' : '' }}"
                                                   name="tuesday_schedule_start">
                                            <label for="tuesday_schedule_start">{{ __('Inicio') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="tuesday_schedule_end" type="text"
                                                   value="{{ $workshop->schedule->tuesday_end }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('tuesday_schedule_end') ? ' is-invalid' : '' }}"
                                                   name="tuesday_schedule_end">
                                            <label for="tuesday_schedule_end">{{ __('Fin') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-md-2">
                                        <h3 class="m-2">Miércoles</h3>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="wednesday_schedule_start" type="text"
                                                   value="{{ $workshop->schedule->wednesday_start }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('wednesday_schedule_start') ? ' is-invalid' : '' }}"
                                                   name="wednesday_schedule_start">
                                            <label for="wednesday_schedule_start">{{ __('Inicio') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="wednesday_schedule_end" type="text"
                                                   value="{{ $workshop->schedule->wednesday_end }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('wednesday_schedule_end') ? ' is-invalid' : '' }}"
                                                   name="wednesday_schedule_end">
                                            <label for="wednesday_schedule_end">{{ __('Fin') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-md-2">
                                        <h3 class="m-2">Jueves</h3>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="thursday_schedule_start" type="text"
                                                   value="{{ $workshop->schedule->thursday_start }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('thursday_schedule_start') ? ' is-invalid' : '' }}"
                                                   name="thursday_schedule_start">
                                            <label for="thursday_schedule_start">{{ __('Inicio') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="thursday_schedule_end" type="text"
                                                   value="{{ $workshop->schedule->thursday_end }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('thursday_schedule_end') ? ' is-invalid' : '' }}"
                                                   name="thursday_schedule_end">
                                            <label for="thursday_schedule_end">{{ __('Fin') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-md-2">
                                        <h3 class="m-2">Viernes</h3>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="friday_schedule_start" type="text"
                                                   value="{{ $workshop->schedule->friday_start }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('friday_schedule_start') ? ' is-invalid' : '' }}"
                                                   name="friday_schedule_start">
                                            <label for="friday_schedule_start">{{ __('Inicio') }}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-label-group">
                                            <input id="friday_schedule_end" type="text"
                                                   value="{{ $workshop->schedule->friday_end }}"
                                                   class="text-center text-uppercase form-control{{ $errors->has('friday_schedule_end') ? ' is-invalid' : '' }}"
                                                   name="friday_schedule_end">
                                            <label for="friday_schedule_end">{{ __('Fin') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <button class="custom-btn btn-block mt-4" type="submit">Guardar</button>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script>

        $('#monday_schedule_start,#monday_schedule_end').mask('LK:00 ZX', {
            translation: {
                'L': {pattern: /[0-1]/},
                'K': {pattern: /[0-9]/},
                'Z': {pattern: /[AP]|[ap]/},
                'X': {pattern: /[M]|[m]/}
            }
        });
        $('#tuesday_schedule_start,#tuesday_schedule_end').mask('00:00 ZX', {
            translation: {
                'Z': {pattern: /[AP]|[ap]/},
                'X': {pattern: /[M]|[m]/}
            }
        });
        $('#wednesday_schedule_start,#wednesday_schedule_end').mask('00:00 ZX', {
            translation: {
                'Z': {pattern: /[AP]|[ap]/},
                'X': {pattern: /[M]|[m]/}
            }
        });
        $('#thursday_schedule_start,#thursday_schedule_end').mask('00:00 ZX', {
            translation: {
                'Z': {pattern: /[AP]|[ap]/},
                'X': {pattern: /[M]|[m]/}
            }
        });
        $('#friday_schedule_start,#friday_schedule_end').mask('00:00 ZX', {
            translation: {
                'Z': {pattern: /[AP]|[ap]/},
                'X': {pattern: /[M]|[m]/}
            }
        });

        if (!$('#monday_schedule_start,#monday_schedule_end').val()) {
            $("#monday_schedule_start,#monday_schedule_end").val("S/H");
        }
        if (!$('#tuesday_schedule_start,#tuesday_schedule_end').val()) {
            $("#tuesday_schedule_start,#tuesday_schedule_end").val("S/H");
        }
        if (!$('#wednesday_schedule_start,#wednesday_schedule_end').val()) {
            $("#wednesday_schedule_start,#wednesday_schedule_end").val("S/H");
        }
        if (!$('#thursday_schedule_start,#thursday_schedule_end').val()) {
            $("#thursday_schedule_start,#thursday_schedule_end").val("S/H");
        }
        if (!$('#friday_schedule_start,#friday_schedule_end').val()) {
            $("#friday_schedule_start,#friday_schedule_end").val("S/H");
        }

    </script>

@endsection