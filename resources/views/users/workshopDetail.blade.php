@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
            <div class="workshop-details">
                <div class="workshop-details-header">
                    <img src="{{ asset('/storage/avatars/'.Auth::user()->avatar) }}" onerror="this.src='{{ asset('/img/workshop-placeholder.png') }}';" alt="img-ws">
                    <div class="workshop-name">
                        <span>{{ $taller->name }}</span>
                    </div>
                </div>
                <div class="workshop-details-body text-center">
                    <h2>Nuestro taller</h2>
                    <h5>{{{ isset($taller->description) ? $taller->description : __('Taller sin descripción') }}}</h5>
                    <h2>Contacto</h2>
                    <ol>
                        <li><strong>Correo electrónico:</strong> {{ $taller->email }}</li>
                        <li><strong>Teléfono:</strong> {{ $taller->phone }}</li>
                        <hr>
                        <li><h2>Horario</h2></li>
                        <li><strong>Lunes:</strong> {{ $taller->schedule->monday_start }} - {{ $taller->schedule->monday_end }}</li>
                        <li><strong>Martes:</strong> {{ $taller->schedule->tuesday_start }} - {{ $taller->schedule->tuesday_end }}</li>
                        <li><strong>Miércoles:</strong> {{ $taller->schedule->wednesday_start }} - {{ $taller->schedule->wednesday_end }}</li>
                        <li><strong>Jueves:</strong> {{ $taller->schedule->thursday_start }} - {{ $taller->schedule->thursday_end }}</li>
                        <li><strong>Viernes:</strong> {{ $taller->schedule->friday_start }} - {{ $taller->schedule->friday_end }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
