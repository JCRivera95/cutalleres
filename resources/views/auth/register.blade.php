@extends('layouts.app')

@section('title', 'Registro')

@section('content')

<style>

    body {
        background: url(/../img/bg-welcome.png) no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100vh;
        margin: 0;
        overflow: hidden;
    }

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-register">
                <div class="card-body">
                    <h5 class="card-title text-center">{{ __('Registro') }}</h5>
                    <form class="form-signin" method="POST" action="{{ route('register') }}">
                        @csrf  
                        
                        <div class="form-label-group">
                            <select class="form-control{{ $errors->has('type_error') ? ' is-invalid' : '' }}" name="type" id="type" required>
                                <option selected="true" disabled="disabled">Selecione una opción...</option>
                                <option value="1">Participar en un taller</option>
                                <option value="2">Ser representante/impartir un taller</option>
                            </select>
                            <label for="type">{{ __('Estoy interesado en') }}</label>
                        </div>

                        <div class="form-label-group" id="workshop_name_form" style="display: none">
                            <input id="workshop_name" type="text" class="form-control{{ $errors->has('workshop_name') ? ' is-invalid' : '' }}" name="workshop_name" value="{{ old('workshop_name') }}">
                            <label for="workshop_name">{{ __('Nombre del taller') }}</label>
                        </div>

                        <div class="form-label-group">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                            <label for="name">{{ __('Nombre') }}</label>
                        </div>

                        
                        <div class="form-label-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            <label for="email">{{ __('Dirección de correo eléctronico') }}</label>
                        </div>
                        
                        <div class="form-label-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <label for="password">{{ __('Contraseña') }}</label>
                        </div>
                        
                        <div class="form-label-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            <label for="password-confirm">{{ __('Confirmar contraseña') }}</label>
                        </div>
                        
                        <hr>
                        <button type="submit" class="btn btn-block">{{ __('Registrar') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

function typeOfUser() {
    var type = $('#type').val();
    console.log(type);
    if (type == 1) {
        $('#workshop_name_form').hide();
    } else {
        $('#workshop_name_form').show();
    }   
}

$('#type').change(typeOfUser);

</script>
@endsection
