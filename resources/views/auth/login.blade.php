@extends('layouts.app')

@section('content')

<style>

    body {
        background: url(/../img/bg-welcome.png) no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100vh;
        margin: 0;
        overflow: hidden;
    }

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin">
                <div class="card-body">
                    <h5 class="card-title text-center">{{ __('Iniciar sesión') }}</h5>
                    <form class="form-signin" method="POST" action="{{ route('login') }}">

                        @csrf
                        
                        <div class="form-label-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            <label for="email">{{ __('Ingrese su dirección de correo eléctronico') }}</label>
                        </div>
                        
                        <div class="form-label-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="password" required>
                            <label for="password">{{ __('Contraseña') }}</label>
                        </div>
                        
                        <div class="custom-control custom-checkbox mb-3 text-center">
                            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember"> {{ __('Recordarme') }}</label>
                        </div>
                        
                        <button class="btn btn-block" type="submit"> {{ __('Entrar') }}</button>
                        <div class="text-center my-2">
                            <a class="small" href="{{ route('password.request') }}">{{ __('Olvidé mi contraseña') }}</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
