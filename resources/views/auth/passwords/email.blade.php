@extends('layouts.app')

@section('content')

<style>

    body {
        background: url(/../img/bg-welcome.png) no-repeat center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height: 100vh;
        margin: 0;
        overflow: hidden;
    }

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin">
                <div class="card-body">
                    <h5 class="card-title text-center">{{ __('Recuperar contraseña') }}</h5>
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form class="form-signin" method="POST" action="{{ route('password.email') }}">

                        @csrf

                        <div class="form-label-group">
                            <input id="email" type="email"
                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                            value="{{ old('email') }}" required>
                            <label for="email">{{ __('Ingrese su dirección de correo eléctronico') }}</label>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Enviar correo de recuperación') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection