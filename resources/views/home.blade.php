@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        @if (!$workshops->isEmpty())
            @foreach($workshops as $workshop)
            <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
                <div class="card-workshop text-center">
                    <a href="{{ route('talleres.show',$workshop->id) }}">
                        <div class="card-workshop-header">
                            <img src="{{ asset('/storage/avatars/'.Auth::user()->avatar) }}" onerror="this.src='{{ asset('/img/workshop-placeholder.png') }}';" alt="img-ws">
                            <p>{{ $workshop->name }}</p>
                        </div>
                        <div class="card-workshop-body text-center">
                            <h5>{{{ isset($workshop->description) ? $workshop->description : __('Taller sin descripción') }}}</h5>
                        </div>
                        <small class="text-muted">Haga clic para ver los detalles del taller</small>
                    </a>
                </div>
            </div>
            @endforeach
        @else
            <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
                <div class="card-workshop text-center">
                    <div class="card-workshop-header">
                        <p>Lo sentimos</p>
                    </div>
                    <div class="card-workshop-body text-center">
                        <h5>Por el momento no hay talleres que mostrar</h5>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

@endsection
